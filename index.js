

// helper function
function alphabetize(word) {
    if (!word) {
        return;
    }
    word = word.split('').sort().join('');
    return word;
}


// main function
function anagramGrouper(words){
    const anagrams = {};
    words.forEach((word)=>{
        const key = alphabetize(word);
        if (anagrams[key]) {
            return anagrams[key].push(word);
        }
        anagrams[key] = [word];
     });
    return anagrams;
}

const groupedAnagrams = anagramGrouper(words);
const br = "<br>"
        const str = "<strong>"
        const strc = "</strong>"
// iterate over groupedAnagrams, printing out each key:value pair on an individual line
document.write(str + "Click button to find words with 5 or more anagrams." + strc)
document.getElementById("countButton").onclick = function () {
    
  for(const key in groupedAnagrams){
        const br = "<br>"
        const str = "<strong>"
        const strc = "</strong>"
        if (groupedAnagrams[key].length < 5) {
            document.write("")
        }else {
        document.write(str + key + strc + ":" + " " + groupedAnagrams[key].toString() + " " + br);
        }
    }
}





   